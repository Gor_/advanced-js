let request = fetch("https://ajax.test-danit.com/api/swapi/films");

request
  .then((result) => result.json())
  .then((films) => {
    films.forEach(({ episodeId, name, openingCrawl, characters }) => {
      document.querySelector(".container").insertAdjacentHTML(
        "afterbegin",
        `<h1>Epizode: ${episodeId}</h1>
        <h2>${name}</h2>
        <p>${openingCrawl}</p>
        <div class = "film-special-card film-card${episodeId}">
        <ul class = "characters${episodeId}"></ul> </div>`
      );

      const charctrs = characters.map((elem) =>
        fetch(elem).then((res) => res.json())
      );

      Promise.allSettled(charctrs).then((data) => {
        data.forEach((elem) => {
          const {
            value: { name },
          } = elem;

          document
            .querySelector(`.characters${episodeId}`)
            .insertAdjacentHTML("beforeend", `<li>${name}</li>`);
        });
      });
    });
  });
