const button = document.querySelector("button");

button.addEventListener("click", detectIP);
async function detectIP() {
  const { ip } = await fetch("https://api.ipify.org/?format=json").then((r) =>
    r.json()
  );

  const { continent, country, region, city, district } = await fetch(
    `http://ip-api.com/json/${ip}`
  ).then((r) => r.json());

  button.insertAdjacentHTML(
    "afterend",
    `<div>Continent: ${continent}</div>
    <div>Country: ${country}</div>
    <div>Region: ${region}</div>
    <div>City: ${city}</div>
    <div>District: ${district}</div>`
  );
}
