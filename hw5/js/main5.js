const postsContainer = document.querySelector(".posts__container");

class Card {
  constructor(id, userId, title, name, email, body) {
    this.id = id;
    this.userId = userId;
    this.title = title;
    this.name = name;
    this.email = email;
    this.body = body;
  }

  createCard() {
    postsContainer.insertAdjacentHTML(
      "afterbegin",
      `<div class="card card-${this.id}">
       <h2 class="card-title">${this.title}</h2>
         <p class="card-body">${this.body}</p>
           <div class="user">
           <h3 class="user-name">${this.name}</h3>
           <a class="user-email">${this.email}</a>
           </div>
           <button class="card-delete">DELETE</button>
    </div>`
    );

    document.querySelector(".card-delete").addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.userId}`, {
        method: "DELETE",
      }).then(() => {
        postsContainer.removeChild(document.querySelector(`.card-${this.id}`));
      });
    });
  }
}

fetch("https://ajax.test-danit.com/api/json/posts")
  .then((response) => response.json())

  .then((res) =>
    res.forEach(({ id, userId, title, body }) =>
      fetch("https://ajax.test-danit.com/api/json/users")
        .then((re) => re.json())

        .then((r) =>
          r.forEach(({ name, email, id: currentUserId }) => {
            if (currentUserId == userId) {
              new Card(id, userId, title, name, email, body).createCard();
            }
          })
        )
    )
  );
