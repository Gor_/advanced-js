class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this.name;
  }
  get age() {
    return this.age;
  }
  get salary() {
    return this.salary;
  }

  set name(nnewName) {
    return (this._name = nnewName);
  }
  set age(newAge) {
    return (this._age = newAge);
  }
  set salary(newSalary) {
    return (this._salary = newSalary);
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super();
    this.name = name;
    this.age = age;
    this.salary = salary * 3;
    this.lang = lang;
  }

  get name() {
    return super.name;
  }
  get age() {
    return super.age;
  }
  get salary() {
    return super.salary * 3;
  }

  set name(nnewName) {
    return (super.name = nnewName);
  }
  set age(newAge) {
    return (super.age = newAge);
  }
  set salary(newSalary) {
    return (super.salary = newSalary);
  }
}

const mark = new Programmer("Mark", 25, 3800, "JS");
const john = new Programmer("John", 32, 4000, "React");
const luke = new Programmer("Luke", 23, 1700, "C++");
console.log(mark);
console.log(john);
console.log(luke);
