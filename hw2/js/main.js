const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const booksList = document.createElement("ul");
document.querySelector("#root").append(booksList);

function checkBooks(book, params) {
  for (const elems of params) {
    if (!book[elems]) {
      throw new Error(`Missing ${elems}`);
    }
  }
}

for (const book of books) {
  try {
    checkBooks(book, ["author", "name", "price"]);
    let bookItem = document.createElement("li");
    bookItem.textContent = `author: ${book.author}, name: ${book.name}, price: ${book.price}`;
    booksList.append(bookItem);
  } catch (error) {
    console.log(error);
  }
}
